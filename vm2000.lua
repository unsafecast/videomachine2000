local colors = require "ansicolors"

function Job(name, duration, video_path, audio_path, replace)
    return {
        name = name,
        duration = duration,
        video_path = video_path,
        audio_path = audio_path,
        replace = true,
    }
end

function ParseResult(val, rest)
    return {
        val = val,
        rest = rest,
    }
end

function read_whole_file(path)
    local file = io.open(path)
    if not file then return nil end
    local content = file:read("*a") .. '\0'
    file:close()
    return content
end

function ident(str)
    local index = 1
    local val = ""
    while str:sub(index, index):match("%w") or str:sub(index, index) == '.' or str:sub(index, index) == '/' do
        val = val .. str:sub(index, index)
        index = index + 1
    end
    
    return ParseResult(val, str:sub(index, #str))
end

function time(str)
    local time = str:sub(1, 6)
    if time == nil then return nil end
    return ParseResult({
        hours = tonumber(time:sub(1, 2)),
        minutes = tonumber(time:sub(3, 4)),
        seconds = tonumber(time:sub(5, 6)),
    }, str:sub(7, #str))
end

function skip_spaces(str)
    local index = 1
    while str:sub(index, index) == ' ' or
          str:sub(index, index) == '\n' or
          str:sub(index, index) == '\r' or
          str:sub(index, index) == '\t' do
        index = index + 1
    end

    return str:sub(index, #str)
end

function read_jobs_file(path)
    local str = read_whole_file(path)
    local jobs = {}

    while str:sub(1, 1) ~= '.' do
        str = skip_spaces(str)

        local result = ident(str)
        if result == nil then
            print(colors("%{bright red}Failed to parse job name%{reset}"))
            return nil end
        local name = result.val
        str = result.rest

        str = skip_spaces(str)

        result = time(str)
        if result == nil then
            print(colors("%{bright red}Failed to parse duration for " .. name .. "%{reset}"))
            return nil
        end
        local duration = result.val
        str = result.rest

        str = skip_spaces(str)

        result = ident(str)
        if result == nil then
            print(colors("%{bright red}Failed to parse video path for " .. name .. "%{reset}"))
            return nil
        end
        local video_path = result.val
        str = result.rest

        str = skip_spaces(str)

        result = ident(str)
        if result == nil then
            print(colors("%{bright red}Failed to parse audio path for " .. name .. "%{reset}"))
            return nil
        end
        local audio_path = result.val
        str = result.rest

        str = skip_spaces(str)

        result = ident(str)
        if result == nil then
            print(colors("%{bright red}No add/replace property for " .. name .. "%{reset}"))
            return nil
        end
        local replace = nil
        str = result.rest

        if result.val == "replace" then replace = true
        elseif result.val == "add" then replace = false end

        if replace == nil then
            print(colors("%{bright red}No add/replace property for " .. name .. "%{reset}"))
            return nil
        end

        str = skip_spaces(str)

        jobs[#jobs+1] = Job(name, duration, video_path, audio_path, replace)
    end

    return jobs
end

function duration_to_str(duration)
    return tostring(duration.hours) .. ":" ..
           tostring(duration.minutes) .. ":" ..
           tostring(duration.seconds)
end

function duration_to_seconds(duration)
    return duration.hours * 60 * 60 + duration.minutes * 60 + duration.seconds
end

function yes_or_no(prompt)
    io.write(colors("%{bright}" .. prompt .. " (%{green}Yes%{reset}%{bright}/%{bright red}No%{reset}%{bright})%{reset} "))
    local answer = io.read("*l")
    if answer == "Yes" then return true
    elseif answer == "No" then return false
    else
        print(colors("%{bright red}Please type %{reset}%{bright}Yes%{reset}%{bright red} or %{reset}%{bright}No%{reset}"))
        return nil
    end
end

function get_duration_of(path)
    os.execute("ffprobe -i " .. path .. " -show_entries format=duration -v quiet -of csv=\"p=0\" > tmp-duration.txt")
    local duration = read_whole_file("tmp-duration.txt")
    local in_seconds = tonumber(duration)
    return in_seconds
end

function loop(path, times, out)
    local file = io.open("tmp-loopfile.txt", "w")
    for i = 0, times do
        file:write("file " .. path .. "\n")
    end
    file:close()
    os.execute("ffmpeg -hide_banner -loglevel error -f concat -i tmp-loopfile.txt -c copy " .. out)
    os.execute("rm tmp-loopfile.txt")
end

function execute_job(job)
    local loops_for_video = math.floor(duration_to_seconds(job.duration) / math.floor(get_duration_of(job.video_path)))
    loop(job.video_path, loops_for_video, "tmp-video.mp4")
    local loops_for_audio = math.floor(duration_to_seconds(job.duration) / math.floor(get_duration_of(job.audio_path)))
    loop(job.audio_path, loops_for_audio, "tmp-audio.mp3")
    local add_or_replace = ""

    if job.replace then add_or_replace = "v"
    else add_or_replace = "0" end
    os.execute("ffmpeg -y -hide_banner -loglevel error -i tmp-video.mp4 -i tmp-audio.mp3 -map 0:" .. add_or_replace .. " -map 1:a -c:v copy -shortest " .. job.name .. ".mp4")
    os.execute("rm tmp-*")
end

function execute_job_old(job)
    os.execute("ffmpeg -hide_banner -loglevel error -i " .. job.video_path .. " -i " .. job.audio_path .. " -map 0:0 -map 1:a -c:v copy -shortest tmp-beforeloop.mp4")

    local loops_for_video = math.floor(duration_to_seconds(job.duration) / math.floor(get_duration_of(job.video_path)))
    local loops_for_audio = math.floor(duration_to_seconds(job.duration) / math.floor(get_duration_of(job.audio_path)))
    local loops = math.min(loops_for_video, loops_for_audio)

    local file = io.open("tmp-loopfile.txt", "w")
    for i = 0, loops do
        file:write("file 'tmp-beforeloop.mp4'\n")
    end
    file:close()
    os.execute("ffmpeg -hide_banner -loglevel error -f concat -i tmp-loopfile.txt -c copy " .. job.name .. ".mp4")

    os.execute("rm tmp-*")
end

function file_exists(path)
    local file = io.open(path)
    if not file then return false end
    file:close()
    return true
end

function main()
    io.write("\027[H\027[2J")
    print(colors("%{green bright}VideoMachine 2000%{reset}"))

    if not arg[1] then
        print(colors("%{bright red}No arguments. Usage: %{reset}%{bright}vm2000 <jobsfile>%{reset}"))
        return nil
    end

    local jobs_file_path = arg[1]
    print(colors("%{bright}Using jobs file %{underline}" .. jobs_file_path .. "%{reset}"))

    local jobs = read_jobs_file(jobs_file_path)
    if jobs == nil then
        print(colors("%{red bright}Jobs file isn't formatted right, please check%{reset}"))
        return nil
    end

    print()

    for job = 1, #jobs do
        if not file_exists(jobs[job].video_path) then
            print(colors("%{bright red}Video file for %{underline}" .. jobs[job].name .. "%{reset} %{bright red}doesn't exist%{reset}"))
            return nil
        end

        if not file_exists(jobs[job].audio_path) then
            print(colors("%{bright red}Audio file for %{underline}" .. jobs[job].name .. "%{reset} %{bright red}doesn't exist%{reset}"))
            return nil
        end
    end

    print(colors("%{bright}Job Queue: %{reset}"))
    for job = 1, #jobs do
        io.write("  " .. jobs[job].name .. ": duration = " .. duration_to_str(jobs[job].duration))
        if jobs[job].replace then
            print("; audio will be " .. colors("%{bright green}replaced%{reset}"))
        else
            print("; audio will be " .. colors("%{bright green}added on top%{reset}"))
        end
    end

    local answer = yes_or_no("Do you want to continue?")
    if answer == nil then return nil end
    if answer == false then
        print(colors("%{bright}Okay, aborting%{reset}"))
        return nil
    end

    print(colors("\n%{bright}From now on, no input is necessary. You can leave if you want to%{reset}"))
    print()

    for job = 1, #jobs do
        print(colors("%{bright}Processing job " .. job .. " out of " .. #jobs .. ": %{underline}" .. jobs[job].name .. "%{reset}"))
        execute_job(jobs[job])
    end

    return {}
end

if main() == nil then
    print()
    print(colors("%{red bright}Something went wrong, all jobs aborted%{reset}"))
else
    print()
    print(colors("%{green bright}All jobs finished successfully.%{reset}"))
end

